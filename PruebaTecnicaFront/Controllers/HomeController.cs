﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;
using PruebaTecnicaFront.Models;
using RestSharp;
using System.Diagnostics;

namespace PruebaTecnicaFront.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            RestClient client = new RestClient("http://localhost:7072/api/GetLibros");
            RestRequest request = new RestRequest("", Method.Get);
            var result = await client.ExecuteAsync(request);
            List<Libro> libro = JsonConvert.DeserializeObject<List<Libro>>(result.Content);
            return View(libro);
        }

        public async Task<IActionResult> Book(int id)
        {
            RestClient client = new RestClient("http://localhost:7072/api/GetLibroDetalle");
            RestRequest request = new RestRequest("", Method.Post);
            GetLibroDetalleRequest req = new GetLibroDetalleRequest()
            {
                idLibro = id
            };
            request.AddJsonBody(JsonConvert.SerializeObject(req));
            var result = await client.ExecuteAsync(request);
            Libro libro = JsonConvert.DeserializeObject<Libro>(result.Content);
            return View(libro);
        }
        public async Task<IActionResult> AgregarLibro()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AgregarLibro(NewLibroRequest requestModel)
        {
            RestClient client = new RestClient("http://localhost:7072/api/CrearLibro");
            RestRequest request = new RestRequest("", Method.Post);
            request.AddJsonBody(JsonConvert.SerializeObject(requestModel));
            var result = await client.ExecuteAsync(request);
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
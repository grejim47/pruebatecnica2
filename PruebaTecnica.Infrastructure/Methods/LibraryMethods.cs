﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Infrastructure.Methods
{
    public class LibraryMethods : ILibraryInfrastructure
    {
        private readonly LibraryContext _context;

        public LibraryMethods(LibraryContext context)
        {
            _context = context;
        }

        public async Task<autores> GetAutoresById(int id) => await _context.autores.Where(x => x.Id == id).FirstOrDefaultAsync();
        public async Task<List<autores>> GetAutores() => await _context.autores.ToListAsync();
        public async Task<List<autores_has_libros>> GetAutoresLibros() => await _context.autores_libros.ToListAsync();
        public async Task<List<libros>> GetLibros() => await _context.libros.OrderBy(x => x.titulo).ToListAsync();
        public async Task<libros> GetLibrosById(int idLibro) => await _context.libros.Where(x => x.ISBN == idLibro).FirstOrDefaultAsync();
        public async Task<List<editoriales>> GetEditoriales() => await _context.editoriales.ToListAsync();
        public async Task<editoriales> GetEditorialesById(int id) => await _context.editoriales.Where(x => x.Id == id).FirstOrDefaultAsync();
        public async Task<List<libros>> GetLibrosByAutorId(int id)
        {
            var id_Libros = await _context.autores_libros.Where(a => a.autores_id == id).Select(x => x.libros_ISBN).ToListAsync();
            return await _context.libros.Where(x => id_Libros.Contains(x.ISBN)).ToListAsync();
        }

        public async Task<autores> InsertAutores(autores autores)
        {
            await _context.autores.AddAsync(autores);
            await _context.SaveChangesAsync();
            return autores;
        }
        public async Task<autores_has_libros> InsertAutores_Libros(autores_has_libros autores_libros)
        {
            await _context.autores_libros.AddAsync(autores_libros);
            await _context.SaveChangesAsync();
            return autores_libros;
        }
        public async Task<editoriales> InsertEditoriales(editoriales editorial)
        {
            await _context.editoriales.AddAsync(editorial);
            await _context.SaveChangesAsync();
            return editorial;
        }
        public async Task<libros> InsertLibros(libros libros)
        {
            await _context.libros.AddAsync(libros);
            await _context.SaveChangesAsync();
            return libros;
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnica.Dtos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaTecnica.Infrastructure.Context
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> options) : base(options)
        {
        }

        public DbSet<autores> autores { get; set; }
        public DbSet<autores_has_libros> autores_libros { get; set; }
        public DbSet<editoriales> editoriales { get; set; }
        public DbSet<libros> libros { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<autores_has_libros>(entity =>
            {
                entity.HasKey(e => new { e.autores_id, e.libros_ISBN });
            });
        }
    }
}
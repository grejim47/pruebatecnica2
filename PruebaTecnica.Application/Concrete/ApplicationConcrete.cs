﻿using Newtonsoft.Json;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Application.Concrete
{
    public class ApplicationConcrete : IAplication
    {
        private readonly ILibraryInfrastructure _libraryInfrastructure;

        public ApplicationConcrete(ILibraryInfrastructure libraryInfrastructure)
        {
            _libraryInfrastructure = libraryInfrastructure;
        }

        public async Task<List<Libro>> GetLibros()
        {
            List<libros> libros = new List<libros>();
            List<autores> autores = new List<autores>();
            List<editoriales> editoriales = new List<editoriales>();
            List<autores_has_libros> librosAutores = new List<autores_has_libros>();
            var task = Task.Run(async () => 
            {
                libros = await _libraryInfrastructure.GetLibros();
                autores = await _libraryInfrastructure.GetAutores();
                editoriales = await _libraryInfrastructure.GetEditoriales();
                librosAutores = await _libraryInfrastructure.GetAutoresLibros();
            });
            task.Wait();
            var query = from libro in libros
                        join editorial in editoriales on libro.editoriales_id equals editorial.Id
                        join relacion in librosAutores on libro.ISBN equals relacion.libros_ISBN
                        select new Libro() { libro = libro, editorial = editorial, autorId = relacion.autores_id };
            query = from libro in query
                    join autor in autores on libro.autorId equals autor.Id
                    select new Libro() { libro = libro.libro, autor = autor, editorial = libro.editorial };
            var json = JsonConvert.SerializeObject(query);
            return query.ToList();
        }
        public async Task<Libro> GetLibroDetalle(GetLibroDetalleRequest request)
        {
            var libros = await GetLibros();
            return libros.Where(x => x.libro.ISBN == request.idLibro).FirstOrDefault();
        }
        public async Task<ResponseAddBook> AgregarLibro(NewLibroRequest request)
        {
            ResponseAddBook response = new ResponseAddBook();
            List<libros> libros = new List<libros>();
            List<autores> autores = new List<autores>();
            List<editoriales> editoriales = new List<editoriales>();
            List<autores_has_libros> librosAutores = new List<autores_has_libros>();
            var task = Task.Run(async () =>
            {
                libros = await _libraryInfrastructure.GetLibros();
                autores = await _libraryInfrastructure.GetAutores();
                editoriales = await _libraryInfrastructure.GetEditoriales();
            });
            task.Wait();
            
            var existeLibro = libros.Where(x => x.titulo == request.titulo).Any();
            if (existeLibro)
            {
                response.mensaje = "Ya existe este libro en nuestra biblioteca";
                return response;
            }
            var autor = autores.Where(a => a.nombre == request.nombreAutor && a.apellidos == request.apellidoAutor).FirstOrDefault();
            if (autor == null)
            {
                autor = new autores()
                {
                    apellidos = request.apellidoAutor,
                    nombre = request.nombreAutor
                };
                await _libraryInfrastructure.InsertAutores(autor);
            }
            var editorial = editoriales.Where(x => x.sede == request.sedeEditorial && x.nombre == request.nombreEditorial).FirstOrDefault();
            if(editorial == null)
            {
                editorial = new editoriales() 
                {
                    nombre = request.nombreEditorial,
                    sede = request.sedeEditorial
                };
                await _libraryInfrastructure.InsertEditoriales(editorial);
            }
            libros libro = new libros()
            {
                editoriales_id = editorial.Id,
                n_paginas = request.n_paginas.ToString(),
                sinopsis = request.sinopsis,
                titulo = request.titulo
            };
            await _libraryInfrastructure.InsertLibros(libro);
            var relacion = new autores_has_libros()
            {
                autores_id = autor.Id,
                libros_ISBN = libro.ISBN
            };
            await _libraryInfrastructure.InsertAutores_Libros(relacion);
            response.respuesta = true;
            response.mensaje = "Libro agregado!!";
            return response;
        }
        public async Task<List<autores>> GetAutores() => await _libraryInfrastructure.GetAutores();
        public async Task<List<editoriales>> GetEditoriales() => await _libraryInfrastructure.GetEditoriales();


    }
}

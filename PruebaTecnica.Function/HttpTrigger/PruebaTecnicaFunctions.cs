using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Infrastructure.Context;
using System.Threading.Tasks;

namespace PruebaTecnica.Function.HttpTrigger
{
    public class PruebaTecnicaFunctions
    {
        private readonly IMediator _mediator;

        public PruebaTecnicaFunctions(IMediator mediator)
        {
            _mediator = mediator;
        }

        [FunctionName("GetLibros")]
        public async Task<IActionResult> GetLibros(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] GetLibrosRequest request, HttpRequest req,
            ILogger log) => new OkObjectResult(await _mediator.Send(request));

        [FunctionName("GetEditoriales")]
        public async Task<IActionResult> GetEditoriales(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] GetEditorialesRequest request, HttpRequest req,
            ILogger log) => new OkObjectResult(await _mediator.Send(request));

        [FunctionName("GetAutores")]
        public async Task<IActionResult> GetAutores(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] GetAutoresRequest request, HttpRequest req,
            ILogger log) => new OkObjectResult(await _mediator.Send(request));

        [FunctionName("GetLibroDetalle")]
        public async Task<IActionResult> GetLibroDetalle(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] GetLibroDetalleRequest request, HttpRequest req,
            ILogger log) => new OkObjectResult(await _mediator.Send(request));
        [FunctionName("CrearLibro")]
        public async Task<IActionResult> CrearLibro(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] NewLibroRequest request, HttpRequest req,
            ILogger log) => new OkObjectResult(await _mediator.Send(request));
    }
}

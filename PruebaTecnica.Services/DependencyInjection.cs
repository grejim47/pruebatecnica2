﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PruebaTecnica.Application.Concrete;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Infrastructure.Context;
using PruebaTecnica.Infrastructure.Methods;

namespace PruebaTecnica.Services
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddMediatR(typeof(PruebaTecnica.Services.DependencyInjection).Assembly);
            services.AddDbContext<LibraryContext>(options => { options.UseSqlServer(Environment.GetEnvironmentVariable("LibraryConnectionString")); });
            services.AddScoped<IAplication, ApplicationConcrete>();
            services.AddScoped<ILibraryInfrastructure, LibraryMethods>();
            return services;
        }
    }
}

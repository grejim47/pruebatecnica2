﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Services.Handlers
{
    public class AgregarLibroHandler : IRequestHandler<NewLibroRequest, ResponseAddBook>
    {
        private readonly IAplication _app;

        public AgregarLibroHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<ResponseAddBook> Handle(NewLibroRequest request, CancellationToken cancellationToken) => await _app.AgregarLibro(request);
    }
}

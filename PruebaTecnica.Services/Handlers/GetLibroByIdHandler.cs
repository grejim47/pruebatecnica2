﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Services.Handlers
{
    internal class GetLibroByIdHandler : IRequestHandler<GetLibroDetalleRequest, Libro>
    {
        private readonly IAplication _app;

        public GetLibroByIdHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<Libro> Handle(GetLibroDetalleRequest request, CancellationToken cancellationToken) => await _app.GetLibroDetalle(request);
    }
}

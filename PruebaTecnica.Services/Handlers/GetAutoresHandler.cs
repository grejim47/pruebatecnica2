﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Services.Handlers
{
    public class GetAutoresHandler : IRequestHandler<GetAutoresRequest, List<autores>>
    {
        private readonly IAplication _app;

        public GetAutoresHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<List<autores>> Handle(GetAutoresRequest request, CancellationToken cancellationToken) => await _app.GetAutores();
    }
}

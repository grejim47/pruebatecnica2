﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Services.Handlers
{
    public class GetLibrosHandler : IRequestHandler<GetLibrosRequest, List<Libro>>
    {
        private readonly IAplication _app;

        public GetLibrosHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<List<Libro>> Handle(GetLibrosRequest request, CancellationToken cancellationToken) => await _app.GetLibros();
    }
}

﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Services.Handlers
{
    public class GetEditorialesHandler : IRequestHandler<GetEditorialesRequest, List<editoriales>>
    {
        private readonly IAplication _app;

        public GetEditorialesHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<List<editoriales>> Handle(GetEditorialesRequest request, CancellationToken cancellationToken) => await _app.GetEditoriales();
    }
}

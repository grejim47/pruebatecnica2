﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PruebaTecnica.Dtos.Models
{
    [Table("autores_has_libros")]
    public class autores_has_libros
    {
        [Key, Column(Order = 0)]
        public int autores_id { get; set; }
        [Key, Column(Order = 1)]
        public int libros_ISBN { get; set; }
    }
}
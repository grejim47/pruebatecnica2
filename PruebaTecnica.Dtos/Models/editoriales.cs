﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaTecnica.Dtos.Models
{
    public class editoriales
    {
        [Key]
        public int Id { get; set; }
        public string nombre { get; set; }
        public string sede { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Response
{
    public class ResponseAddBook
    {
        public bool respuesta { get; set; } = false;
        public string mensaje { get; set; }
    }
}

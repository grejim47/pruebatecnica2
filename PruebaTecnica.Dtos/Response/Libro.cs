﻿using Newtonsoft.Json;
using PruebaTecnica.Dtos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Response
{
    public class Libro
    {
        public libros libro { get; set; }
        public autores autor { get; set; }
        public editoriales editorial { get; set; }
        [JsonIgnore]
        public int autorId { get; set; }
    }
}

﻿using PruebaTecnica.Dtos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Interfaces
{
    public interface ILibraryInfrastructure
    {
        Task<autores> GetAutoresById(int id);
        Task<List<autores>> GetAutores();
        Task<List<libros>> GetLibros();
        Task<libros> GetLibrosById(int idLibro);
        Task<List<editoriales>> GetEditoriales();
        Task<List<libros>> GetLibrosByAutorId(int id);
        Task<autores> InsertAutores(autores autores);
        Task<autores_has_libros> InsertAutores_Libros(autores_has_libros autores_libros);
        Task<editoriales> InsertEditoriales(editoriales editorial);
        Task<libros> InsertLibros(libros libros);
        Task<List<autores_has_libros>> GetAutoresLibros();
        Task<editoriales> GetEditorialesById(int id);
    }
}

﻿using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Interfaces
{
    public interface IAplication
    {
        Task<List<Libro>> GetLibros();
        Task<List<autores>> GetAutores();
        Task<List<editoriales>> GetEditoriales();
        Task<Libro> GetLibroDetalle(GetLibroDetalleRequest request);
        Task<ResponseAddBook> AgregarLibro(NewLibroRequest request);
    }
}

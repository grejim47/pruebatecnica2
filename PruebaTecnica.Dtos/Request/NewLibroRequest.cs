﻿using MediatR;
using PruebaTecnica.Dtos.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Request
{
    public class NewLibroRequest : IRequest<ResponseAddBook>
    {
        public string nombreEditorial { get; set; }
        public string sedeEditorial { get; set; }
        public string titulo { get; set; }
        public string sinopsis { get; set; }
        public int n_paginas { get; set; }
        public string nombreAutor { get; set; }
        public string apellidoAutor { get; set; }
    }
}

﻿using MediatR;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnica.Dtos.Request
{
    public class GetLibrosRequest : IRequest<List<Libro>>
    {
    }
}
